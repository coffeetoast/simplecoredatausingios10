//
//  MainViewController.swift
//  Notes
//
//  Created by SungJae Lee on 2017. 2. 27..
//  Copyright © 2017년 SungJae Lee. All rights reserved.
//

import UIKit
import CoreData

final class MainViewController: UITableViewController {
    fileprivate let cellId = "cellId"
    
    fileprivate lazy var fetchedResultsController: NSFetchedResultsController<Note> = {
        let fetchRequest: NSFetchRequest<Note> = Note.fetchRequest()
        
        let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataManager.shared.persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        
        return fetchedResultsController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        fetchNote()
        setupNaviBar()
        setupTableView()
    }
    
    func fetchNote() {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Save Note")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
    }

    func setupNaviBar() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(handleAdd))
    }
    
    func setupTableView() {
        tableView.allowsMultipleSelection = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    //DataSource.........
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController.sections {
            let sectionInfo = sections[section]
            return sectionInfo.numberOfObjects
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        let note = fetchedResultsController.object(at: indexPath)
        cell.textLabel?.text = note.title
        cell.detailTextLabel?.text = note.content
        return cell
    }
    
    //Delegate ...........
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            CoreDataManager.shared.persistentContainer.performBackgroundTask({ (bmoc) in
                let noteId = self.fetchedResultsController.object(at: indexPath).objectID
                bmoc.delete(bmoc.object(with: noteId))
                
                do {
                    try bmoc.save()
                } catch {
                    let saveError = error as NSError
                    print("Unable to Save Note")
                    print("\(saveError), \(saveError.localizedDescription)")
                }

            })
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let note = self.fetchedResultsController.object(at: indexPath)
        let nVC = NoteViewController()
        nVC.note = note
        navigationController?.pushViewController(nVC, animated: true)
    }
}

extension MainViewController
{
    func handleAdd() {
        let anVC = AddNoteViewController()
        anVC.delegate = self
        
        present(UINavigationController(rootViewController: anVC), animated: true, completion: nil)
    }
}

extension MainViewController: AddNoteViewControllerDelegate
{
    func controller(controller: AddNoteViewController, didAddNoteWithTitle title: String) {
        CoreDataManager.shared.creatNoteBackground(withTitle: title)
    }
}

extension MainViewController: NSFetchedResultsControllerDelegate
{
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath) {
                let note = fetchedResultsController.object(at: indexPath)
                cell.textLabel?.text = note.title
                cell.detailTextLabel?.text = note.content
            }
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
        }
    }

}

//class NoteCell: UITableViewCell
//{
//    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        setupViews()
//        
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//            fatalError("init(coder:) has not been implemented")
//        }
//    
//    func setupViews() {
//        
//    }
//}
