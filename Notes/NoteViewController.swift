//
//  NoteViewController.swift
//  Notes
//
//  Created by SungJae Lee on 2017. 2. 28..
//  Copyright © 2017년 SungJae Lee. All rights reserved.
//

import UIKit

final class NoteViewController: UIViewController {
    
    var note: Note? {
        didSet {
            textField.text = note!.title
            textView.text = note!.content
        }
    }
    
    let textField: UITextField = {
       let tf = UITextField()
        tf.text = "title"
        return tf
    }()
    
    let textView: UITextView = {
       let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 16)
        return tv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNaviBar()
        setupViews()
    }
    
    fileprivate func setupNaviBar() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(self.handleSave))
        
    }
    
    fileprivate func setupViews(){
        view.backgroundColor = .white
        view.addSubview(textField)
        view.addSubview(textView)
        
        textField.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 70, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        textView.anchor(textField.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 20, leftConstant: 20, bottomConstant: 20, rightConstant: 20, widthConstant: 0, heightConstant: 0)
    }

}

extension NoteViewController
{
    func handleSave() {
        if let title = textField.text , let content = textView.text, let noteId = note?.objectID {
            CoreDataManager.shared.persistentContainer.performBackgroundTask({ (bmoc) in
                
                if let note = bmoc.object(with: noteId) as? Note {
                    note.title = title
                    note.content = content
                    note.updatedAt = NSDate()
                }
                
                do {
                    try bmoc.save()
                } catch {
                    let saveError = error as NSError
                    print("Unable to Save Note")
                    print("\(saveError), \(saveError.localizedDescription)")
                }
                
            })

        }
        _ = navigationController?.popViewController(animated: true)
        
    }
}
