//
//  CoreDataManager.swift
//  Notes
//
//  Created by SungJae Lee on 2017. 2. 27..
//  Copyright © 2017년 SungJae Lee. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager
{
    static let shared = CoreDataManager()
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "Notes")
        container.viewContext.automaticallyMergesChangesFromParent = true
        try! container.viewContext.setQueryGenerationFrom(.current)

        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    func creatNoteBackground(withTitle title: String) {
        persistentContainer.performBackgroundTask { (bmoc) in
            let note = Note(context: bmoc)
            
            note.content = ""
            note.title = title
            note.updatedAt = NSDate()
            note.createdAt = NSDate()
            
            do {
                try note.managedObjectContext?.save()
            } catch {
                let saveError = error as NSError
                print("Unable to Save Note")
                print("\(saveError), \(saveError.localizedDescription)")
            }
        }

    }
}
