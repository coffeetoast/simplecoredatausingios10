//
//  ViewController.swift
//  Notes
//
//  Created by SungJae Lee on 2017. 2. 27..
//  Copyright © 2017년 SungJae Lee. All rights reserved.
//

import UIKit

protocol AddNoteViewControllerDelegate {
    func controller(controller: AddNoteViewController, didAddNoteWithTitle title: String)
}

final class AddNoteViewController: UIViewController {

    let titleTextField: UITextField = {
        let ttf = UITextField()
        ttf.placeholder = "text me!"
        ttf.borderStyle = .roundedRect
        //ttf.backgroundColor = .blue
        return ttf
    }()
    
    var delegate: AddNoteViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupNaviBar()
        setupViews()
    }
    
    func setupNaviBar() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.handleCancel))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(self.handleSave))
        
    }
    
    func setupViews() {
        view.backgroundColor = .white
        
        view.addSubview(titleTextField)
        
        titleTextField.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 70, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
    }

}

extension AddNoteViewController
{
    func handleCancel() {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    func handleSave() {
        if let title = titleTextField.text, let delegate = delegate {
            // Notify Delegate
            delegate.controller(controller: self, didAddNoteWithTitle: title)
            
            // Dismiss View Controller
           presentingViewController?.dismiss(animated: true, completion: nil)
        }
        
    }
}

